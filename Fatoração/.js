
(function(global){
    let data, dia, mes, ano,
        inputIn, inputOut,selector;
    function refreschDate(){
            dia  = data.getDate().toString().padStart(2, '0'),
            mes  = (data.getMonth()+1).toString().padStart(2, '0'),
            ano  = data.getFullYear();
    }
function getId(){
    select = $('#select');
    dataIn = $('#dataIn');
    dataOut = $('#dataOut');
}
function inputMask(){
    dataIn.mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
    dataOut.mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
}
function getDaysInMonth(m, y) {
    return m===2 ? y & 3 || !(y%25) && y & 15 ? 28 : 29 : 30 + (m+(m>>3)&1);
}
function selectContent(){
    dataIn.click(()=>{
        dataIn.select();
    });
    dataOut.click(()=>{
    dataOut.select();
    });
}
function eventSelector(){
    select.change((e)=>{
        selector = $(e.target).val();
        inputIn = '',inputOut = '';
        data = new Date();
        switch (selector){
            case '0'://Personalizado
                dataIn.focus();
                break;
            case '1'://Dia
                refreschDate();
                inputIn = `${dia}/${mes}/${ano}`
                setTimeout(()=>{dataIn.select()},1);
                break;
            case '2'://Ontem
                data.setDate(data.getDate()-1);
                refreschDate();
                inputIn = `${dia}/${mes}/${ano}`   
                break;
            case '3'://Ultimos 7 dias
                refreschDate();
                inputOut = `${dia}/${mes}/${ano}`
                data.setDate(data.getDate()-7);
                refreschDate();
                inputIn = `${dia}/${mes}/${ano}`
                break;
            case '4'://Ultimos 30 dias
                refreschDate();
                inputOut = `${dia}/${mes}/${ano}`
                data.setDate(data.getDate()-30);
                refreschDate();
                inputIn = `${dia}/${mes}/${ano}`
                break;
            case '5'://Mês atual
                refreschDate();
                inputIn = `01/${mes}/${ano}`
                inputOut = `${getDaysInMonth(mes,ano)}/${mes}/${ano}`;
                break;
            case '6'://Mês passado
                data.setMonth(data.getMonth()-1);
                refreschDate();
                inputIn = `01/${mes}/${ano}`;
                inputOut = `${getDaysInMonth(mes,ano)}/${mes}/${ano}`;
                break;
            default:
        }
        if(selector !=1 && selector != 2){
            dataOut.show();
            $('#ate').show();
        }else{
            dataOut.hide();
            $('#ate').hide();
        }
        dataIn.val(inputIn)
        dataOut.val(inputOut)
    });

}
function hover(){
    select.tooltip({
        trigger: 'hover',
        title: 'Selecione os filtros já pré definidos!',
        placement: 'top'
    })
    dataOut.tooltip({
        trigger: 'hover',
        title: 'Data fim!',
        placement: 'top'
    })
    dataIn.tooltip({
        trigger: 'hover',
        title: 'Data inicio!',
        placement: 'top'
    })
}
function setSelector(){
    dataIn.keypress(()=>{
        select.val(0)
    })
    dataIn.keypress(()=>{
        select.val(0)
    })
}
getId();
//inputMask();
selectContent();
hover();
eventSelector();
setSelector();
})(this);
    

    
